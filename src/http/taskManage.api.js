import axios from 'axios'
// 获取任务列表
export const getTaskList = payload => {
  const url = '/api/task'
  const params = payload
  return axios.get(url, { params })
}
  // 设置状态
export const setTaskStatus = ({ task, userid, taskid }) => {
  const url = '/api/task/'
  const params = {
    task,
    userid
  }
  return axios.put(url + taskid, params)
}
