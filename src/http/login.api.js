import axios from 'axios'
// 用户登录
export const loginUser = ({ username, password }) => {
  const url = '/api/auth/login'
  const params = {
    username,
    password
  }
  return axios.post(url, params)
}
