import axios from 'axios'
import store from '../store'
axios.defaults.timeout = 5000

axios.interceptors.request.use(
  config => {
    if (store.getters.logined) {
      config.headers.Authorization = `Bearer ${store.getters.logined}`
    }
    return config
  },
  err => {
    return Promise.reject(err)
  })
