// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import FastClick from 'fastclick'
import axios from 'axios'
import 'font-awesome/css/font-awesome.css'
import router from './router'
import store from './store'
import VueRouter from 'vue-router'
import { sync } from 'vuex-router-sync'
import App from './App'

Vue.use(VueRouter)
Vue.prototype.$http = axios
sync(store, router)

require('./http')
FastClick.attach(document.body)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
