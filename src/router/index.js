import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'
import Home from '@/views/Home'
import Login from '@/views/Login'
import Index from '@/views/Index'
import TaskManageUser from '@/views/TaskManage/User'
import TaskManageGroup from '@/views/TaskManage/Group'
import TaskManageCenter from '@/views/TaskManage/Center'
import DailyManageEdit from '@/views/DailyManage/Edit'
import DailyManageAudit from '@/views/DailyManage/Audit'
import DailyManageUser from '@/views/DailyManage/User'
import DailyManageGroup from '@/views/DailyManage/Group'
import DailyManageCenter from '@/views/DailyManage/Center'

Vue.use(Router)
const router = new Router({
  routes: [{
    path: '/',
    name: 'Home',
    component: Home,
    children: [{
      path: 'index',
      component: Index
    }, {
      path: 'task/user',
      name: 'TaskManageUser',
      component: TaskManageUser
    },
    {
      path: 'task/group',
      name: 'TaskManageGroup',
      component: TaskManageGroup
    },
    {
      path: 'task/center',
      name: 'TaskManageCenter',
      component: TaskManageCenter
    },
    {
      path: 'daily/edit',
      name: 'DailyManageEdit',
      component: DailyManageEdit
    },
    {
      path: 'daily/user',
      name: 'DailyManageUser',
      component: DailyManageUser
    },
    {
      path: 'daily/audit',
      name: 'DailyManageAudit',
      component: DailyManageAudit
    },
    {
      path: 'daily/group',
      name: 'DailyManageGroup',
      component: DailyManageGroup
    },
    {
      path: 'daily/center',
      name: 'DailyManageCenter',
      component: DailyManageCenter
    }
    ],
    redirect: './index'
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      skipAuth: true
    }
  }
  ]
})
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.skipAuth !== true)) {
    if (!store.getters.logined) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next()
  }
})
export default router
