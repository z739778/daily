import {
  loginUser
} from '@/http/login.api'
const token = localStorage.getItem('token')
const userInfo = JSON.parse(localStorage.getItem('userInfo'))
console.log(token)
const state = {
  token: token,
  userInfo: userInfo
}
const getters = {
  logined (state) {
    return state.token
  },
  userInfo (state) {
    return state.userInfo
  }
}
const mutations = {
  SAVE_TOKEN (state, payload) {
    state.token = payload
    localStorage.setItem('token', payload)
  },
  SAVE_USER_INFO (state, payload) {
    localStorage.setItem('userInfo', JSON.stringify(payload))
    state.userInfo = payload
  }
}
const actions = {
  handleUserlogin ({ commit }, { username, password }) {
    return loginUser({ username, password })
      .then(({ data }) => {
        commit('SAVE_TOKEN', data.tocken)
        commit('SAVE_USER_INFO', data.userInfo)
      })
  }
}
export default {
  state,
  getters,
  mutations,
  actions
}
