import { getTaskList, setTaskStatus } from '@/http/taskManage.api'

const state = {
  taskList: []
}
const getters = {
  taskList (state) {
    return state.taskList
  }
}
const mutations = {
  SAVE_TASK_LIST (state, payload) {
    state.taskList = payload
  }
}
const actions = {
  getTaskList ({ commit }, payload) {
    return getTaskList(payload)
      .then(({ data }) => {
        commit('SAVE_TASK_LIST', data)
      })
  },
  setTaskStatus ({ commit }, payload) {
    return setTaskStatus(payload)
  }
}
export default {
  state,
  getters,
  mutations,
  actions
}
