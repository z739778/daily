import Vue from 'vue'
import Vuex from 'vuex'
import login from './modules/login'
import taskManage from './modules/taskManage'
Vue.use(Vuex)
const store = new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    login,
    taskManage
  }
})

export default store
